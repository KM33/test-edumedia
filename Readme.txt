I-Cahier des charges
    Considérons une fenêtre rectangulaire de taille 1024px x 768px.
    Le centre de cette fenêtre est matérialisé par un gros point noir fixe.
    Considérons trois disques de couleurs distinctes avec un contour noir.
    Ces disques peuvent être déplacés dans la fenêtre par un simple drag&drop.
    A chaque déplacement, on demande d'afficher dans la fenêtre la couleur du disque le plus proche du centre.
    Ce projet s'effectuera en HTML/CSS/JS en utilisant la balise CANVAS et l'API correspondante.

II- Organisation et gestion du temps.

12h: lecture de la documentation sur les canvas et roadmap.
    Je n'ai personnellement que peu travaillé avec les canvas au cours de ma formation.
    Je me suis donc tourné tout naturellement vers la documentation pour éviter de perdre du temps au cours de la
    rédaction du code.

    Pour organiser mon temps j'ai établi une roadmap:
    -création des éléments HTML/JS
    -gestion du drag&drop
    -calcul de l'élément le plus proche du centre
    -gestion du rendu

    Pas de difficultés rencontrées.

12h30: création des éléments HTML/JS
    Ici, j'ai établi une liste des différentes variables et fonctions auxquelles j'allais avoir recours.
    Cela m'a aidé à organiser mon développement.

    Pas de difficultés rencontrées.

13h: animation
    Pour animer le drag & drop j'ai suivi ce schéma:
    -écouter l'évènement mouse down et obtenir sa position
    -sur cet évènement faire une détection pour savoir si la souris est sur un élément draggable
    -si oui, on drag cet élément
    -écouter l'évènement mouse move up
    -enlever l'attribut draggable à tous les éléments
    -mouse move: repositionner l'élément et établir la position de l'élément suivant la distance parcourue
    -mouse up: on a drop l'élément donc j'arrête d'écouter

    Pas de difficultés rencontrées, cela m'a seulement demandé du temps afin de bien établir la liste des actions à
    faire.

14h: calcul de l'élément le plus proche du centre
    Pour faire cela j'utilise la formule du théorème de Pythagore afin d'obtenir la distance exacte entre deux points.
    J'ai stocké mes trois cercles de couleurs au sein d'un tableau circles sur lequel je boucle de façon à établir
    lequel est le plus proche du centre

    J'ai rencontré ici des difficultés, surtout parce que j'ai exploré plusieurs pistes avant d'arriver à cette solution.
    J'ai d'abord voulu utiliser un tableau associatif pour boucler sur mes éléments grâce à un for in, je suis resté
    bloqué quelque temps dessus avant de me rendre compte que cela m'apportait plus de problèmes sans pour autant me
    donner une solution viable.
    J'ai ensuite voulu utiliser la méthode arr.sort() afin de trier mon tableau suivant les coordonnées de mes objets, puis
    de ressortir celui qui se trouvait le plus proche du centre. Mais le code était lourd, complexe et j'ai trouvé que
    dans une optique d'amélioration du projet, il ne permettait pas par exemple d'ajouter simplement un nouveau disque.


15h: gestion de l'affichage du nom du cercle le plus proche du centre.
       Pas de difficultés particulières.

15h30: refactoring

16h30: écriture du Readme

III- Limites du projet.
    Comme je ne voulais pas rendre ce projet tard, il a des limites au rendu actuel:
    -pas de détection de collision ce qui fait que les cercles peuvent se chevaucher et rester coller
    -parfois les cercles ne répondent pas au clic et ne peuvent être changés de position.

IV- Conclusion.
    Je n'avais pas beaucoup travaillé avec les canvas avant cela, cela fût une très bonne expérience pour moi. Je ne me
    suis pas retrouvé démuni face aux problèmes posés, par contre j'ai pris le temps d'explorer plusieurs solutions qui
    n'étaient certes pas les bonnes, mais qui m'ont permis d'affiner mon code.