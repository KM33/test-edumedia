// je récupère les différentes variables du canvas
let canvas = document.getElementById("canvas");
let ctx = canvas.getContext("2d");
let BB = canvas.getBoundingClientRect();
let offsetX = BB.left;
let offsetY = BB.top;
let WIDTH = canvas.width = 1078;
let HEIGHT = canvas.height = 728;

// je déclare les variables relatives au drag & drop
let dragok = false;
let startX;
let startY;

//je déclare le texte à afficher à l'ouverture de la fenêtre
let text = 'yellow';

circle1 ={
    name: 'blue',
    x: 75,
    y: 50,
    radius: 30,
    startAngle: 0,
    endAngle: Math.PI * 2,
    anticlockwise: false,
    fill: "blue",
    isDragging: false
};
circle2 = {
    name: 'red',
    x: 125 ,
    y: 650,
    radius: 30,
    startAngle: 0,
    endAngle: Math.PI * 2,
    anticlockwise: false,
    fill: "red",
    isDragging: false
};
circle3 = {
    name: 'yellow',
    x: 300,
    y: 200,
    radius: 30,
    startAngle: 0,
    endAngle: Math.PI * 2,
    anticlockwise: false,
    fill: "yellow",
    isDragging: false
};

// je crée un tableau avec mes cercles de façon à pouvoir les utiliser plus facilement plus tard
let circles = [circle1, circle2 ,circle3];

// j'écoute les évènements de la souris
canvas.onmousedown = myDown;
canvas.onmouseup = myUp;
canvas.onmousemove = myMove;



// un simple cercle
function arc(x, y, radius, startAngle, endAngle, anticlockwise) {
    ctx.beginPath();
    ctx.arc(x, y, radius, startAngle, endAngle, anticlockwise)
    ctx.closePath();
    ctx.fill();
}

// on clear
function clear() {
    ctx.clearRect(0, 0, WIDTH, HEIGHT);
}

// on redessine mais cette fois avec tous les cercles
function draw() {
    clear();
    arc(0, 0, WIDTH, HEIGHT);

    for (let i = 0; i < circles.length; i++) {
        ctx.beginPath();
        let c = circles[i];
        ctx.strokeStyle = 'black';
        ctx.lineWidth = 5;
        ctx.fillStyle = c.fill;
        arc(c.x, c.y, c.radius, c.startAngle, c.endAngle, c.anticlockwise);
        ctx.fill();
        ctx.stroke();
    }

    //on dessine le centre à part pour ne pas l'inclure dans le tableau des cercles
    ctx.beginPath();
    ctx.arc(canvas.width / 2, canvas.height / 2, 5, 0, Math.PI *2, false);
    ctx.fillStyle= 'black';
    ctx.fill();
    ctx.stroke();

    ctx.textBaseline = 'top';
    ctx.textAlign = 'left';
    ctx.font = '40px Arial';
    ctx.textAlign = 'center';
    ctx.textBaseline = 'middle';
    ctx.fillText(text, canvas.width / 2, canvas.height / 2 - 40);
}

//je crée une fonction pour afficher le nom du cercle le plus proche dès le début
//fonction que je vais réutiliser au moment de la màj du canvas
function closest() {

    //je déclare le centre de mon canvas
    let myPoint = {
        x: canvas.width / 2,
        y: canvas.height / 2,
    }

//je détermine le point le plus proche de mon centre
    let minDistance = 10000000;
    let closestPoint;
    for (let a = 0; a < circles.length; a++) {
        let distance = Math.sqrt((myPoint.x - circles[a].x) * (myPoint.x - circles[a].x) + (myPoint.y - circles[a].y) * (myPoint.y - circles[a].y));
        if (distance < minDistance) {
            minDistance = distance;
            closestPoint = circles[a];
            //je modifie le texte affiché dans le canvas suivant le cercle le plus proche.
            text = closestPoint.name;
        }

    }

    console.log("The closest point is " +closestPoint.name);

}
draw();

// on gère le clic de la souris
function myDown(e) {

    e.preventDefault();
    e.stopPropagation();

    // on obtient la position actuelle de la souris
    let mx = parseInt(e.clientX - offsetX);
    let my = parseInt(e.clientY - offsetY);

    // on localise si la souris se trouve au sein de l'un des cercles
    dragok = false;
    for (let i = 0; i < circles.length; i++) {
        let c = circles[i];
        if (mx > c.x && mx < c.x + c.radius && my > c.y && my < c.y + c.radius) {

            // si oui, on set le dragging a true
            dragok = true;
            c.isDragging = true;
        }
    }
    // on sauvegarde la position actuelle de la souris
    startX = mx;
    startY = my;

}

// on gère le déclic
function myUp(e) {

    e.preventDefault();
    e.stopPropagation();

    // on set le dragging a false pour tous les cercles
    dragok = false;
    for (let i = 0; i < circles.length; i++) {
        circles[i].isDragging = false;
    }
}

// on gère le déplacement de la souris
function myMove(e) {
    // si on est en train de déplacer l'un des cercles
    if (dragok) {

        e.preventDefault();
        e.stopPropagation();

        // on récupère la position actuelle de la souris
        let mx = parseInt(e.clientX - offsetX);
        let my = parseInt(e.clientY - offsetY);

        //on calcule la distance parcourue par la souris
        //depuis son dernier emplacement
        let dx = mx - startX;
        let dy = my - startY;

        // on déplace chaque cercle qui a l'attribue isDragging
        // suivant la distance parcourue par la souris
        for (let i = 0; i < circles.length; i++) {
            let c = circles[i];
            if (c.isDragging) {
                c.x += dx;
                c.y += dy;
            }
        }

        // on actualise le canvas
        closest();
        draw();

        // ...et on reset la position de la souris
        startX = mx;
        startY = my;

    }
}